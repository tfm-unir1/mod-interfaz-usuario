package com.example.cameracodeexample;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RespuestaActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    BarChart barChart;
    TextView txtResultado;
    ImageView imageview;

    //private String URL = "http://172.19.11.49:8080/mod-comunicacion/webresources/clasificacionGeneracion/obtenerResultado";

    private String URL = "http://192.168.100.16:8383/clasificar_generar";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_respuesta);
        Intent intent = getIntent();
        String imgBase64 = intent.getStringExtra(MainActivity.IMG_ENTRADA);
        Log.d("Resultado", imgBase64 + "***");
        //imgBase64 = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCACyALIDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDoB1oIxS4OaCM0AJSd6dikxzQAn8Ipp6VJikK0ARnoPrSjpTgORTXKouSQBnqaAGn6Zpo+6Mc1zGteO9N0wvBB/pNyOCE5VPqa4u98falcyEoFhU/wrzSuPlPWieT19+OlQTXlta/6+4ij/wB9wv8AOvDLrVbu8P724c7vvfMcVU3PnO4Y+maVx8qPeRqlg/3byA/8DFTo6SIHjdXU9CpyK8Qt7aaUBncqD0UHHFXYpr60cSwXF0jZ4aOU5pcw+U9jx/n0pCOK8+0vx3fQfJqMa3cYPMiDa6j1I6Gu303VLTV7UT2ku9f4l6Mp9CP8+2eKpO5NmWCKaRUxFRkUxEZFMK1MRTCKAISKYRU5FMIoAh20VLtooA3iooxTyKTFADcUmKfijbQBHilx7U7bg1DczpbwNJI4VV5JJx9aAIby7gsLV7m5cRxIMkn+X1rybxL4zu9XleC1LwWgyBsOC/1NM8W+J21i5aON2WyQkKCfvEd65cuDwn61DbL5bEZRz34z0HSpFt3Mm05yeTxSornA+6PetixhVSCJCDnoKBmatiTuADAgZ5q5HYrCxckcHj/ZHvWrb6apldo87gQwGOvNGoxx2sbQkhpRnLL0X2FZt62LUdLszZLxYgRHEGIOCXHanRXUuN7kAA9VXFZzIZJcKWb/AIEP69av2kUwxlST6Hg/nTEWJUjljLFVJPAIOT/nmn6TqUuk3nmxscfpj/PUfqOKsxRJJFhgVyOQe3+ePyrPuLdrZyRjZ6enr+FGw79z1jTNRi1OyWeI9eGX+6fQ/wBKuYzmvLdE1eTTLtCGxE/J9CP89K9NtblLqJXVgSRn61pGVzKUbEhFNIqXg0hFUSQEU0ipiKaRQBD+FFSbfaigDdxRt4qTbQRnjFAEe3pRtp5WkPHv9KAIXYKp459K8s8c+J/PdtPtpB5anErDoT2X6V0/jTxEdMs2t4j/AKRNlRg8qK8buXaWU5bP9aiT1si4rS7IyTK+48joAe1P6dsn0p8cLFQQMD1NOaNV43Z+lK5S1IhlMndznHy1paehb+I9euaqRQhmCojZYZz71tadZtGytxjHcdalsaVzbsWAuYwMNtx0H86qa/Zl7yRgeCxPJwK3NJtf9I3noozwOv8AjWPrULy3jsG7+vNZX965rb3bHJyNcISEfaPUZP5c1es72SLBkxIMZ4HJpzRxROF3lnB5CrmrSC2woDrlh0bA/wDritTOxeikWdS6kBhyeOn+f88c1WuYnQ5yCvp6H/CpIbcRlWTIweO4P0P59f5VckAcHjDEYx2xU3Glc59swncVzEfvKev4en0rqfC+r+VKtpI+6JjmOTP3T6H0rGlt13lGGUB79QPf3qgQ2nzt08pjkg9Pzpp9ga7nsYdsblG4Y6DrUikMMg1g+GtWXULOMNIWlX5XJ7+9dABk4x26VsnfYwaa3GEU0rU5WoyKYiP8aKft9qKAN7bQRzUm2kK0AREVWvriOztJZ5DhVUnmrTsqDcxGOxz/AJ/z78V53488QIYBZ28hbfgkjoR7ev8AKlKVioq7OH16/fUtQkupj99mKr6L2/l+grH4ByFAH60skpkfcf1qIk5Azye/tWK11NnpoK7sxITJ+pp9vA08iKCevUUxE8zgcIP1ra0u1LuNi/jQ2kKKbNvRdBWXHTp1rr7Xw1EE5HI6ZpmhWojRd3HFdXCoK4xXFUqNvQ7qdNJGK2mNAgSJVGeuKyLrw08pDEnp1xzXdpCGAyARVwWkTIPlHSoU2U4I8rn8Gkodu/OOQFNctqehT2O5kjAwOw/ya+hINMgc5KDn2rF8QaBDJGzIoz6Y61pGo1uZTgmeEWd7NFIyMzoQe3OPr/8AqNaSXS4US42sBiQdOcdfT/6wrQ1bQ2t5WeOIdeRj+R6istlzFgHaxBOPUDPI9eCf/wBWa6U0zncWizKAyCQDJTriopbZLiPyzwMZRu4qG2n2HGTleCD0Pv8A5/DPFXIXjztJ+UjPP8JobsNK5V0e7l0vU1WTJiZsH0+tep2kyzQqQcjgg+oIzXmtza+ap3f6yPkMerD+prs9BnZtPt5MkbcoxPqOn6cVdOWtiKkdLnQEZphFSAhhkdOKQg+lbnOR49qKdg+9FAG/UNzNHbxFpGwAPzqaVhGhYjpWbchdhaQ73+8ccgAHj8qAMXWbmea3eSVvKiBJVTySPfHX8c/nXkGp3Rvr2aQFmDHbk9WHvXYeL9da8m+xW7HBIVmBwcc8fqPyrjJcKWVSKxnI3hEpONqkt/KoGJAJ7sPyFWZlOQo54yapTNyAD8xNJAy7bIG5P3RwK7PRLYYUkc1y2l23nyqoxtjP6122niNMfMFx6kVjVlrZHRSjpdnVWKBAuBW5DyuM1iaeyui4OfpW/bJ8tcbudUSzG3FaMIJQd6oYI5FXoWO0UkNl6AZAJ6YouoFmTBJzToeFpzsABzzVmXU4PXtJID8A5HJxXlWvQNbStxg56jv/AIV77qEC3EZA5yMV5j4t0IiGWVFyF5K9aqnNxeopw5kea295+9DHAI/iIwCP5f57cVrQPuGVGc/jXPXANpdHspyQ3p/WtO0lfeHUfe6845//AF11S7nNE6GA+dFtY7tpCqe4Hr+HP6/jt+HZ0R5rUj74HToev9P5Vz9vhWQ8gEckdga00b96shBBztyByCO2enofzFZqVmaON9DurT/UAFm+U7Tn1FTEVl6RdpcK78bjyw9xwf5fma1utdsXdHFNWYzB9aKd+VFUSal2zKBjtz+GK53Xb4Wtt9njJM7/ADMBydvf/P8A9bO5ql0trDvHzkHke2P8n14rz7xbc/YNMk8z95c3Zw+48gkdPy/z1zMnYqKuzhLy78y4luHPzyEn2zVHePmcjrnAI5qKRwzbOcAnNPUnGcH2IrnOlaEcnyhsnlutUA6m5ZuoXgVLcXILFU+YjHNMt4yTuI5zWiVjNu51Gi2zLp6YyGfkmt+LS7RIhI900ZH8RPANR6NZh7dFPZcVV12xuX2qgLRg/dBrmTvLU6WuWOxONSvLOXNjqlnMo6YkAJ/A9uneui0vxpdKwS8tQVOTvUdsfzrhr/ToJJ7dorNyMJmIo2SQckMRzz65zWwdOXT7eGWze4Mp5mh8lyACSVG4gAkAge+O1aShGxnGpJM9UsdUjv4ldCQD61prIYl3t90dTXnnhu6dLtYmyM9vSvRry336fgDGVrikrSsdid0c9f8Aj20sZDDDG88o4wg6n61lJ4o1zWJ9kdqttGcAvK+3H9SKqeIbV4CltFNHaq/Dyl8FR7Z5rgJNP8zWbiH7S02cpA6TLgtu+UknjbjuCK6acFY56k2mey2dg8qkz6plyckR5+X/AIESBT5tOcRtC9wbiMjgsACPyriLLTLu28QeTpMtzcWSciVl24OenON31wM9s16Xb2Ui2waXO7FY1Uk9DSm7o8D8U6T9hv5LcjAY/IcZ49KxLN9iNG4w6fKwJ6D/APXXpvxH01ZBFOEJIOK86RQWCyqcjo4Gf/1V0U3zQMZrlmb9gwmjUB8E9yPXrkdfxrTjmNuFSZN0bDa5BGQo6cemK5uzmEHJ2SRg53Akbf5ED6muijljuIlVgMHhW9/X69KzkmtzWLT2NrTZvseoxkvvjcbTuIB254OP88812EYwi5HbNefWsmyE2Uz4ReYt2OM9fw6fiM/XrtE1M3MBt5DumhGG/wBodm/z7Vvh5/ZZzV4dUauz2opOP7v6UV1HKOvUM96kG/KoQZCO5OSPY9M9cjArx3xjqC3niG543wxOVBB4IA+v97+Zrv7+x1O6srq9luljRsyjYxHK5Ax/eGOuRXj16nzE5AUsTk9Tg1lOTNoRRE3zszuwAP3iaiuJyQUVSFB5P9PpSuHlbJ4UdM1EQpbC4Y+p7GoSLbIFTceBgVZhcCREzxnmmP8AKuOpqKJ1W4RpCQoZdx9BV7ojZnp2jzAKgHcCustrGO5AyM1xGlsY1VSfmXGf8/lXdaTMQBXnVLpnpxXNE0odLjhTATr1x3qpqMEcUR2qA2MdPf09a2UmHlf/AF6yNXf90zVKlYHBoxtMhVNShKdS3Oa9PQBrWNW5GK8x0cefqCFeQGr0wnEEXtQ+7Bq2hQ1DSkuRuZQykYPGP/r1Ws/D9msgk8qMsOhK8it2KaNsqSM+lPaJc5UDFOLdtybW6Edvp8EABRFXHTaMfWi5bYhA6YqZZgi4rMvp+CAamQRicF46dWtApPQk9a8xuGjLkEZAHJH+eK77xfcxqLq4uD+5tYx0/ic9APzFeOvqE/2qWUOVMjZO012YZXic2JkkzYZCH3QTMpA4UH39+lT2d9NGwjkYjLcBjgH3FZcbvKQHckt0Pv6VLkyrhwCwPIPf6HtWrSe5km1sdjbXQmVFeP5l+6f7vt9K0ILx7G5ivI2XKE5wfvL3H06VxljqTW7BW+dO3Zl+tdNaTRX0f7iVFcjBD8A8Vg48rujdNSVmehLq9syBt78jP3f/AK9FcCLG+AAEK4/31orb6wYexR33iC5WLQbptoKhWXC8KuRwM/jXhUshZ9+OvVjyeteueObwW2hvbquCxVEJ5zk5YfgF/WvIZtwJTq35VpUZFPYgkYvkA8dsmkCqicYAPf1qcx/wg59ahcBj754FSimQOSx9+1VZeDirpXcCxx/k1SlHzZq4kM7jw1eve2haQfOhKsc/e46/lx+Fd1psxXBrzTwY53XMf8IKn+Yr0Ox5A5rhrpKbR6GHleCZ1MEu9etZetzEQsBU8U2xcZ5qvdlZgdxGD61zrc3k0ytocsUF+gLgAmvRjfWy2qB5FBHXmvONN0uFr5QGwD1Y9K7XTNP8wyJJGXRTlSRnI6VT1C2l2W54idtxA24deKtw3BKAcmpgAECAZHSqzKFB25FQQpXCWUnJHFZly7MWPp1q27gqQKqS42En8aGy27I8O8a6y+o6ldWG0LBBK4II5LDIyfy6e1cXNDsI+v8AStrUrpbrWrudMhJZ2ccdixNUZE3qrFAASen1r06fuxseXN88ri25PkIy/wALY685rQuogSJY+cgHAGOCAcfqP19Kz7ZS9vIvHBwPXrmtG1nBhgDkZ2sv0IPT8iBTe4LYrEBiXGfqOv8A9ertrK8eG4Ze5X61WuIHgYuAQhx1BqxB8xzGw3D+E9M1L1KWjNX+2Ln/AJ+ZPzoqt5Z/59n/ADorPkj2Nby7m74y1NtR19ogxEUBJUDkDIBPA78Vx2ze5Y4znHWta7lkmd5pmLSO298nODWdkRw+ZgHnCAjkmtG7syUbIrSsq/Ivr196rspCH3yT9KmCYG9hnJJ570yYDcq84wKYiMg7ApAHGaoyLkn0NaBOXbvj0qpIMFV74qosmSNHwpciDVfLY4Eq7QPU5r0yzk24wa8egZklDglWByCO1em6Tfme0hkcAOVBbHrWGJjrc6MNLSx1hGY1cHnHNYeo314JtgtiynkFWHIrShn3R4B7U1rcznac56g46GuWDs9TrIdMlu3k2ShoAcY3rkfmD9a10u723iMlndvKy/Iqxq3zc/X1zTrNntodjIAA24MBx/nrWvpl6YZSViVlZQoAHTAxkH8625omlluirpOu60smLjS7pt3GximSfb5v54roEuJ51BlhaFj/AAMRn9Kfb4TnYAzcdP8AOamZQQc4H0rCbT2IlLUr54AFc3401uPRNAnl3gTOPKhH+0wx+nJ+grX1K+g06B555VSNBliT04//AF14T4v8SzeJNQMvItbdsxRn0HBb6n9KqjS5pXexjWmox8zC3Frh8ehH6cVbEeIoz1LAcfX/APVVaEYnderZB4+nT+VaWFYg7s7RuBY9sk/1rvOJGfYMDPKCuN3I/T+hq9FF5UTbXwyzBQCMZBAOf0FUYX236ZUgBcHA9sf4VtCJXWVt24vGpGRjBVhuIz0+WlIcRzRrLaByoyDyABkZ/wA/yqiYtpLA4yCAc9q0bUgBoiuMjOCOO4Yfp/Kq7p5cwXOTkAg9M5qbl2E8yf8Auj86Kd5lp/eP5UUBck1K0lsblIZQqyEZKofu/wD18Vn3pwsca9AoJPoTn/Cp9Vv5L3UXnk+8zZwOn+cVSu5C8mPTj+f+NHUOhAGLyqMnaowOaST55hk9B/KmqT5nHApu7M0hPABPX6j/ABqiWO3DBPTIqnLJ8xPfnFSyPthyfXiq+wyK7Doo559x/iKuKIk9LBCrSyKq9WIAr0SxXYiKvG0YFcfoVmZZxMw+VSQPc121qnArnxEtUjow8bK5p285Ugc10OnTRtjdjNc8kJK5HBq1au64DHn1rlZ1nbWMSSyHIU1vQQQgABV49q4yx1B4gMn8q0Y9UnLfKpqAaOkk8tcgdRWfdXm07I+XPT/69U0murk4ztHfFTGFYYiTyf50C2PMPijfyxpZ2Zk3CUO7KOxAG3+ZrzCI78ggc56+4rqfiRfi58YmMZxBEsePc/N/JhXHKSj4J6V6VKCjA4KzvOxoWoJlRv8AZyceoFX2BWQANkEKMGqFrjzM7tvX9RWgdvmEg8BhjND3EjNBzcuysV+bqe3H/wBaugiKNGAWBTJBx1I6dfTGa56dfLuZMgj5j936/wD6/wA62LSYvbhem5doJGMDGKUhxLEMjBGVTtdJPyJIz/I1LepkZQgDGR3OMjH17flVFZlzICAC3zZ9f85qSOf9ztYgloVA9Rhj0/OpL6E/lD/nmfzFFIs7FQdw5HpRRqBkO4NyDnv/APrqKUhnPYdai84LMT/Wo55lGcHrzVJak30FZwsi8+9Ix/eP3Geffkf4VCqmQkjIX/PrVmC1mupP3UZbPcdKt2W5K12Ks7fKqgjgVat7OW6WGBVwCNzMBzjJ/wA/hWta+GWY7p2LHPC9q6bT9HSDB24P+cVlKsor3S40XJ+8ULSyFtHGirgAYrbtIScUxrcmbgVqWdvgjiuS99WdXL0RYhtzgcVKtvtbkVo28A2cin/Zv3owKhs0Qy3tgMdRWrbwKBnGaS3hJHTNacVtlRnikFwgXA4GKhv3KwnBxWgkKoucE/U1l6l+8LKOlAup4P45jEniGeYR4ZSMsT94YGPx6/lXNmHc3yjnIB+ld74zsSt5JL5XmRsCGX8Dgg+vT8q46KPDJ2zjNehTqXgjinT99sgtiVmyeeMVryAeZLn7p5BFZxhEbJLypH3gRjHNX4yCY8KArLgZ9f8APH1qm7kpFS9XEpPc4b9KdaS7VIyCAeCDkVYulSS0WQg7lJU/5/CqliBJL5fDZyAM98cfn/jR0BbkhkIHOSc46dqcsrKQSScJgfhVe6QxXBHQZ/n0pVbJUn0xUlGit6wUDb0HtRVXantRSGY7GiUnA57UUV0GBs6NGkmN6K3+8M11tnGigAIoHsKKK46m51wNSFRtHAq8gG0cCiisGbIhwPO6Vp2wGRxRRSGbFv8AdqQ/6wUUVBSNGL7wq/FRRQJliT7lZE/RqKKBI4DxKoLvkA156VAvV4H3qKK2pbE1NyLGbibPqf5UsP8AqF9m/oD/ADoorqWxyPcW453g8jP/ALNWbYk/bl5/iH8qKKvoT1NPWwPLiOBn5v61mQ9vrRRUR2Le5ZooopDP/9k=";
        clasificarGenerar(imgBase64);
        /*byte[] decodedString = Base64.decode(imgBase64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        imageview = (ImageView)findViewById(R.id.imageView);
        imageview.setImageBitmap(decodedByte);*/
    }

    private void clasificarGenerar(final String imagenEntrada) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("imagenEntrada", imagenEntrada);

        JsonObjectRequest request_json = new JsonObjectRequest(URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            if ("1".equals(response.getJSONObject("respuesta").getString("codigo"))) {
                                Toast.makeText(RespuestaActivity.this, response.getJSONObject("forma").getString("resultado"), Toast.LENGTH_LONG).show();
                                txtResultado = (TextView) findViewById(R.id.textResultado);
                                txtResultado.setText(response.getJSONObject("forma").getString("resultado"));
                                if (response.getJSONObject("forma").getString("resultado").length() < 10) {
                                    cargarResultado(response);
                                    cargarImagenResultado(response);
                                }
                            } else {
                                Toast.makeText(RespuestaActivity.this, response.getJSONObject("respuesta").getString("mensaje"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Resultado", error.getMessage());
                        progressDialog.dismiss();
                        Toast.makeText(RespuestaActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request_json);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Procesando....");
        progressDialog.show();
    }

    public void cargarResultado(JSONObject response) throws JSONException {
        barChart = (BarChart) findViewById(R.id.barchart);
        BarDataSet barDataSet = new BarDataSet(getData(response), "Formas faciales");
        barDataSet.setBarBorderWidth(0.8f);
        barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        BarData barData = new BarData(barDataSet);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        final String[] formas = new String[]{"Diamante", "Ovalado", "Alargado", "Cuadrado", "Redondo", "Triángulo"};
        IndexAxisValueFormatter formatter = new IndexAxisValueFormatter(formas);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(formatter);
        barChart.getDescription().setText("Porcentaje de forma facial");
        barChart.setData(barData);
        barChart.setFitBars(true);
        barChart.animateXY(2000, 2000);
        barChart.invalidate();
    }

    private void cargarImagenResultado(JSONObject response) throws JSONException {
        byte[] decodedString = Base64.decode(response.getJSONObject("forma").getString("resultadoImg"), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        imageview = (ImageView)findViewById(R.id.imageView);
        imageview.setImageBitmap(decodedByte);
    }

    private ArrayList getData(JSONObject response) throws JSONException {
        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(0f, Float.valueOf(response.getJSONObject("forma").getString("diamante"))));
        entries.add(new BarEntry(1f, Float.valueOf(response.getJSONObject("forma").getString("ovalado"))));
        entries.add(new BarEntry(2f, Float.valueOf(response.getJSONObject("forma").getString("alargado"))));
        entries.add(new BarEntry(3f, Float.valueOf(response.getJSONObject("forma").getString("cuadrado"))));
        entries.add(new BarEntry(4f, Float.valueOf(response.getJSONObject("forma").getString("redondo"))));
        entries.add(new BarEntry(5f, Float.valueOf(response.getJSONObject("forma").getString("triangulo"))));
        return entries;
    }
}
